﻿using System;
using System.Collections.Generic;

namespace Assignment_5
{
    class Program
    {
        static void Main(string[] args)
        {
            // initilizes some animals
            IslandHorse Horse1 = new IslandHorse(new HashSet<string>() { "gray", "black" }, 190, "Horsey");
            NormalHorse Horse2 = new NormalHorse("Reginald", new HashSet<string>() { "brown" }, 120, "Unknow");

            Horse1.Drink();
            Horse2.Drink();
            Horse1.Eat();
        }
    }
}
