﻿using System;
using System.Collections.Generic;


namespace Assignment_5
{
    public class Animal
    {
        public int Height { get; set; }
        
        public HashSet<string> Colors { get; set; } 

        public Animal(int Height, HashSet<string> Colors)
        {
            this.Height = Height;
            this.Colors = Colors;
        }

        public void Jump()
        {
            Console.WriteLine("The animal jumped");
        }

    }
}
