﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5
{
    public abstract class Horse : Animal
    {
        public string Name { get; set; }

        public Horse(int Height, HashSet<string> Colors, string Name) : base (Height, Colors) 
        {
            this.Name = Name;
        }

        public virtual void Neigh()
        {
            Console.WriteLine("The horse neighed");
        }
    }
}

